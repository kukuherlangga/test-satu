-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 30, 2021 at 09:10 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `karyawan`
--

-- --------------------------------------------------------

--
-- Table structure for table `datasalary`
--

CREATE TABLE `datasalary` (
  `id` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `tunjangan` int(11) NOT NULL,
  `id_gol` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `datasalary`
--

INSERT INTO `datasalary` (`id`, `jumlah`, `tunjangan`, `id_gol`) VALUES
(1, 4200000, 300000, '3A'),
(2, 4000000, 200000, '3B'),
(3, 5500000, 700000, '4A'),
(4, 3800000, 100000, '2A');

-- --------------------------------------------------------

--
-- Table structure for table `data_transaksi`
--

CREATE TABLE `data_transaksi` (
  `Id_tran` int(11) NOT NULL,
  `Id_kar` int(11) NOT NULL,
  `Jml_Lembur` int(11) NOT NULL,
  `Total_Lembur` int(11) NOT NULL,
  `Total_Salary` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `data_transaksi`
--

INSERT INTO `data_transaksi` (`Id_tran`, `Id_kar`, `Jml_Lembur`, `Total_Lembur`, `Total_Salary`) VALUES
(1, 2, 0, 0, 0),
(2, 3, 6, 0, 0),
(3, 1, 2, 0, 0),
(4, 6, 0, 0, 0),
(5, 5, 4, 0, 0),
(6, 4, 0, 0, 0),
(7, 10, 3, 0, 0),
(8, 9, 6, 0, 0),
(9, 8, 3, 0, 0),
(10, 7, 7, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `karyawan`
--

CREATE TABLE `karyawan` (
  `Id` int(11) NOT NULL,
  `Nama` text NOT NULL,
  `Alamat` varchar(100) NOT NULL,
  `Status` varchar(50) NOT NULL,
  `Tgl_masuk` date NOT NULL,
  `Masa_kontrak` int(50) NOT NULL,
  `Jabatan` varchar(50) NOT NULL,
  `Grade` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `karyawan`
--

INSERT INTO `karyawan` (`Id`, `Nama`, `Alamat`, `Status`, `Tgl_masuk`, `Masa_kontrak`, `Jabatan`, `Grade`) VALUES
(1, 'Bayu', 'Lampung', 'Tetap', '2021-01-12', 0, 'Tech. Support', '3B'),
(2, 'Budi', 'Jakarta', 'Kontrak', '2021-05-24', 15, 'Spv. IT', '4A'),
(3, 'Andi', 'Yogyakarta', 'Kontrak', '2020-01-12', 6, 'Programmer', '3A'),
(4, 'Yuyu', 'Bandung', 'Kontrak', '2020-07-15', 12, 'Operator', '2A'),
(5, 'Mijan', 'Tegal', 'Tetap', '2021-01-04', 0, 'Operator', '2A'),
(6, 'Sukija', 'Tegal', 'Kontrak', '2020-04-30', 12, 'Operator', '2A'),
(7, 'Lia', 'Palembang', 'Kontrak', '2020-05-12', 12, 'Staff MR.', '3A'),
(8, 'Okti', 'Padang', 'Tetap', '2020-01-12', 0, 'Tech. Support', '3B'),
(9, 'Ika', 'Padang', 'Kontrak', '2020-05-28', 12, 'Staff Payroll', '3A'),
(10, 'Siman', 'Tegal', 'Kontrak', '2020-06-12', 12, 'Programmer', '3A');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `karyawan`
--
ALTER TABLE `karyawan`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Id` (`Id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `karyawan`
--
ALTER TABLE `karyawan`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
