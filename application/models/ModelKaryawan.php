<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ModelKaryawan extends CI_Model {
  function __construct(){
    parent::__construct();
  }
  public function getDataKaryawan($filter=[]){
    if(empty($filter)){
      return $this->db->get('karyawan')->result_array();
    }else{
      if(isset($filter['habis_kontrak'])){
        return $this->db->query('
          SELECT * FROM (
            SELECT
              K.Id,
              K.Nama,
              K.Status,
              K.Tgl_masuk,
              K.Masa_kontrak,
              (DATE_ADD(K.Tgl_masuk, INTERVAL K.Masa_kontrak MONTH)) AS TanggalHabisKontrak,
              (TIMESTAMPDIFF(
                DAY,
                NOW(),
                DATE_ADD(K.Tgl_masuk, INTERVAL K.Masa_kontrak MONTH)
              )) as SisaHari
            FROM karyawan K
            WHERE K.status = "Kontrak"
          ) AS K WHERE SisaHari BETWEEN 1 AND 31
        ')
        ->result_array();
      }
    }
  }
  public function getJumlahKaryawan($filter = []){
    $ids = [];
    if(isset($filter['total_salary'])){
      $dataTransaksi = $this->getDataTransaksi();
      foreach($dataTransaksi as $index => $row){
        if($row['Total_Salary'] >= 4500000){
          array_push($ids,$row['Id_kar']);
        }
      }
    }

    $db = $this->db;
    if(isset($filter['status']) && in_array($filter['status'], ['Tetap','Kontrak'])){
      $db->where('Status',$filter['status']);
    }
    if(isset($filter['golongan'])){
      $db->where('Grade',$filter['golongan']);
    }
    if(isset($filter['total_salary'])){
      $db->where_in('Id',$ids);
    }
    return $db->get('karyawan')->num_rows();
  }
  public function getDataSalary(){
    return $this->db->get('datasalary')->result_array();
  }
  public function getDataTransaksi(){
    return $this->db
    ->select('
      T.Id_tran,
      T.Id_kar,
      T.Jml_Lembur,
      (T.Jml_Lembur * 35000) AS Total_Lembur,
      (S.jumlah + S.Tunjangan + (T.Jml_Lembur * 35000)) AS Total_Salary
    ')
    ->from('data_transaksi T')
    ->join('karyawan K','K.Id = T.Id_kar')
    ->join('datasalary S','S.id_gol = K.Grade')
    ->get()->result_array();
  }
}
