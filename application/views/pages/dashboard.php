<h2><?=$title?></h2>
<hr>
<div class="row">
  <div class="form-group col-md-12">
  </div>
</div>
<hr>
<div class="row">
  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
    <div class="card rounded-0 p-0 shadow-sm">
      <div class="card-body text-center">
        <h6 class="card-title">KARYAWAN HABIS KONTRAK 1 BULAN KEDEPAN</h6>
        <hr class="pb-2"/>
        <ul class="list-group" id="list_karyawan_habis_kontrak"></ul>
      </div>
    </div>
  </div>
  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
    <div class="card rounded-0 p-0 shadow-sm">
      <div class="card-body text-center">
        <h6 class="card-title">JUMLAH KARYAWAN YANG STATUS KARYAWAN KONTRAK GOLONGAN 3A DENGAN TOTAL SALARY DIATAS Rp 4.500.000,-</h6>
        <hr class="pb-2"/>
        <div class="text-center" style="font-size:24px" id="jumlah_karyawan_kontrak_3a"></div>
      </div>
    </div>
  </div>
  <div class="col-xs-6 col-sm-3 col-md-3 col-lg-2">
    <div class="card rounded-0 p-0 shadow-sm">
      <div class="card-body text-center">
        <h6 class="card-title">JUMLAH KARYAWAN YANG STATUS KONTRAK</h6>
        <hr class="pb-2"/>
        <div class="text-center" style="font-size:24px" id="jumlah_karyawan_kontrak"></div>
      </div>
    </div>
  </div>
  <div class="col-xs-6 col-sm-3 col-md-3 col-lg-2">
    <div class="card rounded-0 p-0 shadow-sm">
      <div class="card-body text-center">
        <h6 class="card-title">JUMLAH KARYAWAN YANG STATUS TETAP</h6>
        <hr class="pb-2"/>
        <div class="text-center" style="font-size:24px" id="jumlah_karyawan_tetap"></div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    let data_karyawan_habis_kontrak_1_bulan = []
    function drawDataKaryawan(){
      let html = ''
      data_karyawan_habis_kontrak_1_bulan.map((row, index) => {
        html += `
          <li class="list-group-item d-flex justify-content-between align-items-center">
            [${row.Id}] ${row.Nama}
            <span class="badge badge-primary badge-pill">${row.SisaHari} Days Left</span>
          </li>
        `
      })

      $('#list_karyawan_habis_kontrak').html(html)
    }
    function getData(){
      $.ajax({
        url:'<?=base_url("/api/get_karyawan")?>',
        method:'get',
        dataType:'json',
        data:{
          habis_kontrak:true
        },
        success:function(response){
          if(response.code == 1){
            data_karyawan_habis_kontrak_1_bulan = response.data
            drawDataKaryawan()
          }else{
            alert('Error get data')
          }
        }
      })
      $.ajax({
        url:'<?=base_url('/api/jumlah_karyawan')?>',
        method:'get',
        dataType:'json',
        data:{status:'Kontrak'},
        success:function(response){
          if(response.code == 1){
            $('#jumlah_karyawan_kontrak').html(response.data + ' Karyawan')
          }else{
            alert(response.msg || 'Error')
          }
        }
      })
      $.ajax({
        url:'<?=base_url('/api/jumlah_karyawan')?>',
        method:'get',
        dataType:'json',
        data:{status:'Tetap'},
        success:function(response){
          if(response.code == 1){
            $('#jumlah_karyawan_tetap').html(response.data + ' Karyawan')
          }else{
            alert(response.msg || 'Error')
          }
        }
      })
      $.ajax({
        url:'<?=base_url('/api/jumlah_karyawan')?>',
        method:'get',
        dataType:'json',
        data:{status:'Kontrak',total_salary:4500000,golongan:'3A'},
        success:function(response){
          if(response.code == 1){
            $('#jumlah_karyawan_kontrak_3a').html(response.data + ' Karyawan')
          }else{
            alert(response.msg || 'Error')
          }
        }
      })
    }

    getData()
  })
</script>