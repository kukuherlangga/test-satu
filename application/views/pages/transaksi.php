<h2><?=$title?></h2>
<hr>
<div class="row">
  <div class="form-group col-md-12">
    <table class="table table-bordered" id="table_transaksi_salary">
      <thead>
        <tr>
          <th>Id_tran</th>
          <th>Id_kar</th>
          <th>Jml_Lembur</th>
          <th>Total_Lembur</th>
          <th>Total_Salary</th>
        </tr>
      </thead>
      <tbody></tbody>
    </table>
    <a href="#" class="btn btn-primary btn-refresh">Refresh</a>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    let dataTransaksi

    function drawDataToTable(){
      let html_table_transaksi_salary = ''
      dataTransaksi.map((row, index) => {
        html_table_transaksi_salary += `
        <tr>
          <td>${row.Id_tran}</td>
          <td>${row.Id_kar}</td>
          <td>${row.Jml_Lembur}</td>
          <td>${row.Total_Lembur.formatMataUang()}</td>
          <td>${row.Total_Salary.formatMataUang()}</td>
        </tr>
        `
      })
      $('#table_transaksi_salary tbody').html(html_table_transaksi_salary)
    }

    function getData(){
      $.ajax({
        url:'<?=base_url("/api/get_transaksi")?>',
        method:'get',
        dataType:'json',
        success:function(response){
          if(response.code == 1){
            dataTransaksi = response.data
            drawDataToTable()
          }else{
            alert(response.msg || "Error")
          }
        }
      })
    }

    $('.btn-refresh').on('click',getData)

    getData()
  })
</script>