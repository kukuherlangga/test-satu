<h2><?=$title?></h2>
<hr>
<div class="row">
  <div class="form-group col-md-12">
    <table class="table table-bordered" id="table_karyawan">
      <thead>
        <th>Id_kar</th>
        <th>Nama</th>
        <th>Alamat</th>
        <th>Status</th>
        <th>Tgl_masuk</th>
        <th>Masa_kontrak</th>
        <th>Jabatan</th>
        <th>Grade</th>
      </thead>
      <tbody></tbody>
    </table>
    <a href="#" class="btn btn-primary btn-refresh">Refresh</a>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    let dataKaryawan

    function drawDataToTable(){
      let html_table_karyawan = ''
      dataKaryawan.map((row, index) => {
        html_table_karyawan += `
        <tr>
        <td>${row.Id}</td>
        <td>${row.Nama}</td>
        <td>${row.Alamat}</td>
        <td>${row.Status}</td>
        <td>${row.Tgl_masuk}</td>
        <td>${row.Masa_kontrak.formatMasaKontrak()}</td>
        <td>${row.Jabatan}</td>
        <td>${row.Grade}</td>
        </tr>
        `
      })
      $('#table_karyawan tbody').html(html_table_karyawan)
    }

    function getData(){
      $.ajax({
        url:'<?=base_url("/api/get_karyawan")?>',
        method:'get',
        dataType:'json',
        success:function(response){
          if(response.code == 1){
            dataKaryawan = response.data
            drawDataToTable()
          }else{
            alert(response.msg || "Error")
          }
        }
      })
    }

    $('.btn-refresh').on('click',getData)

    getData()
  })
</script>