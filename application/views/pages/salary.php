<h2><?=$title?></h2>
<hr>
<div class="row">
  <div class="form-group col-md-12">
    <table class="table table-bordered" id="table_salary">
      <thead>
        <th>Id_salary</th>
        <th>Jml_salary</th>
        <th>Tunjangan</th>
        <th>Id_gol</th>
      </thead>
      <tbody></tbody>
    </table>
    <a href="#" class="btn btn-primary btn-refresh">Refresh</a>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    let dataSalary

    function drawDataToTable(){
      let html_table_salary = ''
      dataSalary.map((row, index) => {
        html_table_salary +=`
        <tr>
          <td>${row.id}</td>
          <td>${row.jumlah.formatMataUang()}</td>
          <td>${row.tunjangan.formatMataUang()}</td>
          <td>${row.id_gol}</td>
        </tr>
        `
      })
      $('#table_salary tbody').html(html_table_salary)
    }

    function getData(){
      $.ajax({
        url:'<?=base_url("/api/get_salary")?>',
        method:'get',
        dataType:'json',
        success:function(response){
          if(response.code == 1){
            dataSalary = response.data
            drawDataToTable()
          }else{
            alert(response.msg || "Error")
          }
        }
      })
    }

    $('.btn-refresh').on('click',getData)

    getData()
  })
</script>