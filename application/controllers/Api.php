<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {
  function __construct(){
    parent::__construct();
    $this->load->model(['ModelKaryawan']);
  }
  public function get_karyawan(){
    $filter = $this->input->get();
    echo json_encode([
      'code'=>1,
      'msg'=>'Success Get Data',
      'data'=>$this->ModelKaryawan->getDataKaryawan($filter)
    ]);
  }
  public function get_transaksi(){
    echo json_encode([
      'code'=>1,
      'msg'=>'Success Get Data',
      'data'=>$this->ModelKaryawan->getDataTransaksi()
    ]);
  }
  public function get_salary(){
    echo json_encode([
      'code'=>1,
      'msg'=>'Success Get Data',
      'data'=>$this->ModelKaryawan->getDataSalary()
    ]);
  }
  public function jumlah_karyawan(){
    $filter = $this->input->get();
    echo json_encode([
      'code'=>1,
      'msg'=>'Success Get Data',
      'data'=>$this->ModelKaryawan->getJumlahKaryawan($filter)
    ]);
  }
}
