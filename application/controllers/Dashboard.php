<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	function __construct(){
		parent::__construct();
	}
	public function index(){
		$this->load->view('layouts/header');
		$this->load->view('pages/dashboard',[
			'title'=>'Dashboard',
		]);
		$this->load->view('layouts/footer');
	}
	public function list_karyawan(){
		$this->load->view('layouts/header');
		$this->load->view('pages/list_karyawan',[
			'title'=>'List Karyawan',
		]);
		$this->load->view('layouts/footer');
	}
	public function salary(){
		$this->load->view('layouts/header');
		$this->load->view('pages/salary',[
			'title'=>'List Salary',
		]);
		$this->load->view('layouts/footer');
	}
	public function transaksi(){
		$this->load->view('layouts/header');
		$this->load->view('pages/transaksi',[
			'title'=>'List Transaksi',
		]);
		$this->load->view('layouts/footer');
	}
}
