jQuery(function ($) {
  $(".sidebar-dropdown > a").click(function() {
    $(".sidebar-submenu").slideUp(200);
    if (
      $(this)
      .parent()
      .hasClass("active")
      ) {
      $(".sidebar-dropdown").removeClass("active");
    $(this)
    .parent()
    .removeClass("active");
  } else {
    $(".sidebar-dropdown").removeClass("active");
    $(this)
    .next(".sidebar-submenu")
    .slideDown(200);
    $(this)
    .parent()
    .addClass("active");
  }
});
  $("#close-sidebar").click(function() {
    $(".page-wrapper").removeClass("toggled");
  });
  $("#show-sidebar").click(function() {
    $(".page-wrapper").addClass("toggled");
  });
});

// Format mata uang dari angka / number
String.prototype.formatMataUang = function(){
  let value = this.toString().split('').reverse().join('')
  let returnValue = ''
  for(let i = 0; i < value.length; i++ ){
    if((i+1)%3 == 0){
      if(i==value.length-1){
        returnValue+=value[i]
      }else{
        returnValue+=value[i]+'.'
      }
    }else{
      returnValue+=value[i]
    }
  }
  returnValue = returnValue.split('').reverse().join('')
  return 'Rp '+returnValue+',-'
}

String.prototype.formatMasaKontrak = function(){
  let value = this
  if(value >= 12){
    let year = Math.floor(value/12)
    let month = value - (year*12)
    if(month>0){
      return year + ' Tahun ' + month + ' bulan'
    }else{
      return year + ' Tahun'
    }
  }else if(value == null || value == 0){
    return 'NULL'
  }else{
    return this + ' bulan'
  }
}